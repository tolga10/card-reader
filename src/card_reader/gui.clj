(ns card-reader.gui
  (:require [seesaw.core :as sc]
            [seesaw.mig :as sm])
  (:require (clj-time [core :as time] [coerce :as tc]
                      [local :as l]))
  (:use card-reader.db))

(defn text-field []
  (let [txt-field (sc/text "")]
    txt-field))


(declare welcome-panel)

(defn recorder-panel [txt-field]
  (let [f (sc/frame :title "RFID Card Recorder")
        p (sm/mig-panel
           :constraints ["fill" "[|grow]"]
           :items [[(sc/label "Lutfen kartinizi okutunuz") "wrap"]
                   [txt-field ""]])]
    (.setColumns txt-field 10)
    (sc/listen txt-field :key-typed (fn [e]
                                      (when (= (.getKeyChar e) \newline)
                                        (sc/dispose! f)
                                        (welcome-panel txt-field))))
    (sc/config! f :content p)
    (-> f sc/pack! sc/show!)))


(defn welcome-panel [txt-field]
  (future
    (let [f (sc/frame :title "Welcoming Screen")
          card-id (.getText txt-field)
          p (sm/mig-panel
             :constraints ["fill" "[|grow]"]
             :items [[(sc/label (apply str "Sayin " card-id  ", Hosgeldiniz.")) ""]])]
      (insert-into-db card-id (tc/to-sql-date (l/local-now))
                      (tc/to-sql-time (l/local-now)))
      (sc/config! f :content p)
      (-> f sc/pack! sc/show!)
      (Thread/sleep 5000)
      (sc/dispose! f)
      (recorder-panel (text-field)))))

