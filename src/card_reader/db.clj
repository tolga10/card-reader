(ns card-reader.db
  (:require [clojure.java.jdbc :as sql]))


(def db-spec
  {:classname "com.mysql.jdbc.Driver"
   :subprotocol "mysql"
   :subname "//localhost:3306/cardreader"
   :user "cardreader"
   :password "cardreader"})


(defn reset-db []
  (try
    (sql/db-do-commands db-spec
                        (sql/drop-table-ddl :records))
    (catch Exception e (println e)))
  (sql/db-do-commands
   db-spec
   (sql/create-table-ddl
    :records
    [:recordID :integer "PRIMARY KEY" "AUTO_INCREMENT"]
    [:cardID "varchar(30)"]
    [:date :DATE]
    [:time :TIME])))


(defn insert-into-db [card-id date time]
  (try
    (sql/insert! db-spec :records
                 {:cardID card-id
                  :date date
                  :time time})
    (catch Exception e (println e))))
